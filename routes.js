//define and set database variables
var mongojs = require("mongojs");
var databaseName = "mach_test";
var databaseUri = "mongodb://localhost/" + databaseName;
var collections = ["test_collection"];
var db = mongojs.connect(databaseUri, collections);

//other variables
var item = require("./dao/item.js");
var user = require("./dao/user.js");
var Busboy = require("connect-busboy");
var path = require("path");
var fs = require("fs");
var mkdirp = require("mkdirp");

//-------- Index page routing --------
exports.index = function(req, res) {
    res.sendfile("./public/views/index.html");
},   

//-------- Admin functions routing - Static --------    
exports.admin = function(req, res) {
    res.sendfile("./public/views/admin/index.html");
},
    
exports.adminPanel = function(req, res) {
    if(!req.session.loggedIn) {
        res.status(500).sendfile("./public/views/admin/500.html");
    }
    else {
        res.status(200).sendfile("./public/views/admin/adminPanel.html");    
    }
},

exports.adminPOST = function(req, res) {
    req.pipe(req.busboy);
    var username;
    var password;

    req.busboy.on('field', function(fieldname, value) {
        console.log(fieldname + " : " + value);
        if(fieldname == "username") {
            username = value;
        }
        else if(fieldname == "password") {
            password = value;

            if(username == "admin" && password == "admin123") {
                console.log("logging in user");
                req.session.loggedIn = true;
                console.log(req.session);
                res.status(200).sendfile('./public/views/admin/adminPanel.html');            
            }
            else {
                res.sendfile("./public/views/admin/500.html");
            }
        }

    });
},
  
//-------- Get items from database routing --------  
exports.getItem = function(req, res) {
 
    this.requestId = req.param("id");
    
    item.getRecordById(db, this.requestId, function(result, message) {
        if(result ==  false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });
    
},

exports.getAllItems = function (req, res) {
 
    item.getRecord(db, null, function(result, message) {
        if(result ==  false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });   
},


exports.getItemByParams = function(req, res) {
    
    this.key = req.param("key");
    this.value = req.param("value");
    
    item.getRecordByOtherField(db, this.key, this.value, function(result, message) {
        if(result == false) {
            res.status(500).json(message);   
        }
        else {
            res.status(200).json(message);   
        }
    });
    
},

//-------- Delete items from database routing --------  
exports.deleteItem = function(req, res) {
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, value) {
        if(fieldname == "delete-id") {
            item.deleteRecordById(db, value, function(result, message) {
                if(result) {
                    res.status(200).json(message);
                }
                else {
                    res.status(500).json(message);
                }
            });        
        }
    });
    
},

exports.deleteItemByParams = function(req, res) {
    var key;
    var value;
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, fieldvalue) {
        if(fieldname == "delete-key") {
            key = value;
        }
        else if(fieldname == "delete-value") {
            value = fieldvalue;

            item.deleteRecordByOtherField(db, key, value, function(result, message) {
                if(result) {
                    res.status(200).json(message);
                }
                else {
                    res.status(500).json(message);
                }
            });
        }
    });

}

//-------- Edit items in database routing --------  
exports.editItem = function(req, res) {
    var id;
    var key;
    var value;
    var createNew;
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, fieldvalue) {
        if(fieldname == "edit-id") {
            id = fieldvalue;          
        }
        else if(fieldname == "edit-key") {
            key = fieldvalue;
        }
        else if(fieldname == "edit-value") {
            value = fieldvalue;
        }
        else if(fieldname == "edit-create-new-flag") {
            createNew = fieldvalue;

            item.editRecordById(db, id, key, value, createNew, function(result, message) {
                if(result) {
                    res.status(200).json(message);
                }
                else {
                    res.status(500).json(message);
                }
        });
        }  
    });
},

exports.editItemByParams = function(req, res) {
    var id;
    var qKey;
    var qValue;
    var sKey;
    var sValue;
    var createNew;
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, fieldvalue) {
        if(fieldname == "edit-id") {
            id = fieldvalue;          
        }
        else if(fieldname == "edit-query-key") {
            qKey = fieldvalue;
        }
        else if(fieldname == "edit-query-value") {
            qValue = fieldvalue;
        }
        else if(fieldname == "edit-set-value") {
            sKey = fieldvalue;
        }
        else if(fieldname == "edit-set-value") {
            sValue = fieldvalue;
        }
        else if(fieldname == "edit-create-new-flag") {
            createNew = fieldvalue;

            item.editRecordById(db, id, qKey, qValue, sKey, sValue, createNew, function(result, message) {
                if(result) {
                    res.status(200).json(message);
                }
                else {
                    res.status(500).json(message);
                }
        });
        }  
    });
},

//-------- User's operations routing --------  
exports.loginUser = function(req, res) {
    var username;
    var password;
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, value) {
        if(fieldname == "username") {
            username = value;
        }
        else if(fieldname == "password") {
            password = value;

            user.getRecordByOtherField(db, "username", username, function(result, records) {
                if(result) {
                    if(records.password == password) {
                        req.session.username = records.username;
                        console.log("user logged in successfully: " + records.username);
                        res.status(200).json({message: records.username + " is logged in!"});
                        //res.status(200).sendfile("./public/welcome.html");
                    }
                    else {
                        console.log("failed login for: " + records.username + " password: " + password);
                        res.status(500).json({message: "Invalid username or password!"});
                        //res.status(500).sendfile("./public/invalid_login.html");
                    }
                }
                else {
                    console.log("failed login for: " + records.username + " password: " + password);
                    res.status(500).json({message: "Invalid username or password!"});
                    //res.status(500).sendfile("./public/invalid_login.html");
                }
            });
        }
    });
},

exports.createUser = function(req, res) {
    var avatar;
    req.pipe(req.busboy);
    req.busboy.on("field", function(fieldname, value) {
        if(fieldname == "first-name") {
            user.setFirstName = value;
        }
        else if(fieldname == "last-name") {
            user.setLastName = value;
        }
        else if(fieldname == "user-name") {
            user.setUserName = value;
        }
        else if(fieldname == "password") {
            user.setPassword = value;
        }
        else if(fieldname == "privileges") {
            user.setPrivileges = value;
        }
    });
    req.busboy.on("file", function(fieldname, file, filename, encoding, mimetype) {
        if(fieldname == "avatar") {
            var savePath = "";
            var fullPath = path.join(savePath, filename);
            this.uploadFile(file, path, function(result, message) {
                if(result) {
                    user.setAvatar = fullPath;
                    user.insertRecord(db, function(result, message) {
                        if(result) {
                            req.session.username = user.getUserName();
                            console.log("user created successfully: " + user.getUserName(););
                            res.status(200).json({message: user.getUserName(); + " has created a new account!"});
                            //res.status(200).sendfile("./public/welcome.html");
                        }
                        else {
                            console.log("unsuccessful account creation: " + user.getUserName(););
                            res.status(500).json({message: "there was an error creating your account"});
                            //res.status(500).sendfile("./public/500.html");   
                        }
                    });        
                }
                else {
                    console.log("error occured when saving avatar. error: " + message);
                    res.status(500).json({message: "there was an error uploading your avatar. your account was not created."});
                    //res.status(200).sendfile("./public/500.html");   
                }
            });
            
        }
    });
},

//-------- Test functions routing --------  
exports.insertAlbumTest = function(req, res) {
    album.setName("test name");
    album.setDescription("test description");
    
    album.insertRecord(db, function(result, message) {
        if(result == false) {
            res.status(500).json(message);
        }
        else {
            res.status(200).json(message);
        }
    });  
},

exports.insertPictureTest = function(req, res) {
    picture.setName("test picture");
    picture.setDescription("test description");
    picture.setUrl("/images/image01.png");
    this.pictureId = req.param("albumId");
    
    var result = picture.insertRecord(db, function(result, message) {
        if(result == false) {
            res.status(500).json(message);   
        }
        else {
            //res.status(200).json({status:"success", message:message});   
            result = album.addPicture(db, this.pictureId, message, function(result, message) {
                if(result == false) {
                    res.status(500).json(message);   
                }
                else {
                    res.status(200).json(message);   
                }
            });
        }    
    });    
},

exports.selectTestAlbum = function(req, res) {
 
    album.getRecord(db, null, function(result, message) {
        if(result ==  false) {
            res.status(500).json({status:"fail", message:message});   
        }
        else {
            res.status(200).json({status:"success", message:message});   
        }
    });
},

//-------- Utility functions --------  
uploadFile = function(file, path, callback) {
    mkdirp(path, function(err) {
        if(err) {
            //res.status(500).json({message:"error creating path: " + err});
            callback(false, {message:"error creating path: " + err});
        }
        else {
            var ostream = fs.createWriteStream(path);
            file.pipe(ostream);
            ostream.on("close", function(err) {
                if(err) {
                    //res.status(500).json({message:"error saving file: " + err});           
                    callback(false, {message:"error saving file: " + err});
                }
                else {
                    callback(true, {message:"file saved successfully"});
                }
            });
        }
    });
}
