/*------------------------------------------
Database access object for the photo 
collection. 

Able to set the required 
fields for insertion and use other functions
such as delete and getting records
-------------------------------------------*/

//-------- Document field setters --------
exports.setFirstName = function(n) {
    this.firstName = n;   
}

exports.setLastName = function(n) {
    this.lastName = n;   
}

exports.setUserName = function(u) {
    this.userName = u;   
}

exports.setPassword = function(p) {
    this.password = p;   
}

exports.setPrivileges = function(p) {
    this.privileges = p;
}

exports.setAvatar = function(a) {
    this.avatar = a;
}

exports.getUserName = function() {
    return this.userName;
}

exports.getFirstName = function() {
    return this.firstName;
}
//-------- CRUD functions --------
exports.insertRecord = function(dbObj, callback) {
 
    this.document = {
        first_name : this.firstName,
        last_name : this.lastName,
        username : this.userName,
        password : this.password,
        privileges : this.privileges,
        avatar : this.avatar
    };
    
    dbObj.users.insert(this.document, function(err, record) {
        if(err) {
            callback(false, err);   
        }
        callback(true, record._id);
    });
},
// pass null to idValue to get back all the records    
exports.getRecordById = function(dbObj, idValue, callback) {
 
    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.criteria = {_id:dbObj.ObjectId(idValue)};
    }
    
    dbObj.users.find(this.criteria, function(err, recordSet) {
       
        if(err) {
            callback(false, err);   
        }
        callback(true, recordSet);
        
    });
},

exports.getRecordByOtherField = function(dbObj, key, value, callback) {
    if(key == null || value == null) {
        var message = "Both key and value must be set. You sent, key: " + key + " value: " + value;
        callback(false, message);
    }
    else {
        this.criteria = {key:value};
    }
    dbObj.users.find(this.criteria, function(err, recordSet) {
        if(err) {
            callback(false, err);
        }
        callback(true, recordSet);
    });
}

exports.deleteRecordById = function(dbObj, idValue, callback) {

    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.criteria = {_id:dbObj.ObjectId(idValue)};
    }

    dbObj.users.remove(this.criteria, function(err, result, lastError) {
        console.log("err: " + err);
        console.log("result: " + result);
        console.log("lastError: " + lastError);

        if(err) {
            callback(false, err);
        }
        callback(true, result);
    });   

}

exports.deleteRecordByOtherField = function(dbObj, key, value, callback) {
    if(key == null || value == null) {
        var message = "Both key and value must be set. You sent, key: " + key + " value: " + value;
        callback(false, message);
    }
    else {
        this.criteria = {key:value};
    }
    dbObj.users.remove(this.criteria, function(err, recordSet) {
        if(err) {
            callback(false, err);
        }
        callback(true, recordSet);
    });
}

//createNew: if set to true, will create a new record if the search criteria doesn't match
exports.editRecordById = function(dbObj, idValue, key, value, callback) {
    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.query = {_id:dbObj.ObjectId(idValue)};
        this.update = {key:value};
    }
    dbObj.users.update(
        this.query, 
        {$set: this.update},
        function(err, result) {
            if (err) {
                callback(false, err);
            };
            callback(true, result);
        }
    );
}

//createNew: if set to true, will create a new record if the search criteria doesn't match
exports.editRecordByOtherField = function(dbObj, qKey, qValue, sKey, sValue, callback) {
    if(qKey == null || qValue == null) {
        var message = "Both key and value must be set. You sent, key: " + qKey + " value: " + qValue;
        callback(false, message);
    }
    else {
        this.query = {qKey:qValue};
        this.update = {sKey:sValue};
    }
    dbObj.users.update(
        this.query, 
        {$set: this.update},
        function(err, result) {
            if (err) {
                callback(false, err);
            };
            callback(true, result);
        }
    );
}

exports.checkCredentials = function(dbObj, username, password, function) {
    if(username == null || password == null) {
        var message = "Both username and password must be set. You sent, username: " + username + " password: " + password;
        callback(false, message);
    }
    else {

    }
}