/*------------------------------------------
Database access object for the photo 
collection. 

Able to set the required 
fields for insertion and use other functions
such as delete and getting records
-------------------------------------------*/

//-------- Document field setters --------
exports.setCollectionName = function(cN) {
    this.collectionName = cN;
}

exports.setTitle = function(t) {
    this.title = t;   
}

exports.setName = function(n) {
    this.name = n;   
}

exports.setDescription = function(d) {
    this.description = d;   
}

exports.setUrl = function(u) {
    this.url = u;   
}

exports.setPictureThumb = function(t) {
    this.thumb = t;
}

exports.setPictureThumbUrl = function(t) {
    this.thumbUrl = t;
}

//-------- CRUD functions --------
exports.insertRecord = function(dbObj, callback) {
 
    this.document = {
        name : this.name,
        title : this.title,
        description : this.description,
        url : this.url,
        thumbnail : this.thumb,
        thumbnail_url : this.thumbUrl
    };
    
    dbObj.this.collectionName.insert(this.document, function(err, record) {
        if(err) {
            callback(false, err);   
        }
        callback(true, record._id);
    });
},
// pass null to idValue to get back all the records    
exports.getRecordById = function(dbObj, idValue, callback) {
 
    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.criteria = {_id:dbObj.ObjectId(idValue)};
    }
    
    dbObj.this.collectionName.find(this.criteria, function(err, recordSet) {
       
        if(err) {
            callback(false, err);   
        }
        callback(true, recordSet);
        
    });
},

exports.getRecordByOtherField = function(dbObj, key, value, callback) {
    if(key == null || value == null) {
        var message = "Both key and value must be set. You sent, key: " + key + " value: " + value;
        callback(false, message);
    }
    else {
        this.criteria = {key:value};
    }
    dbObj.this.collectionName.find(this.criteria, function(err, recordSet) {
        if(err) {
            callback(false, err);
        }
        callback(true, recordSet);
    });
}

exports.deleteRecordById = function(dbObj, idValue, callback) {

    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.criteria = {_id:dbObj.ObjectId(idValue)};
    }

    dbObj.this.collectionName.remove(this.criteria, function(err, result, lastError) {
        console.log("err: " + err);
        console.log("result: " + result);
        console.log("lastError: " + lastError);

        if(err) {
            callback(false, err);
        }
        callback(true, result);
    });   

}

exports.deleteRecordByOtherField = function(dbObj, key, value, callback) {
    if(key == null || value == null) {
        var message = "Both key and value must be set. You sent, key: " + key + " value: " + value;
        callback(false, message);
    }
    else {
        this.criteria = {key:value};
    }
    dbObj.this.collectionName.remove(this.criteria, function(err, recordSet) {
        if(err) {
            callback(false, err);
        }
        callback(true, recordSet);
    });
}

//createNew: if set to true, will create a new record if the search criteria doesn't match
exports.editRecordById = function(dbObj, idValue, key, value, createNew, callback) {
    if(idValue == null) {
        callback(false, "ID value must be specified");
    }
    else {
        this.query = {_id:dbObj.ObjectId(idValue)};
        this.update = {key:value};
    }
    dbObj.this.collectionName.update(
        this.query, 
        {$set: this.update},
        {upsert: createNew},
        function(err, result) {
            if (err) {
                callback(false, err);
            };
            callback(true, result);
        }
    );
}

//createNew: if set to true, will create a new record if the search criteria doesn't match
exports.editRecordByOtherField = function(dbObj, qKey, qValue, sKey, sValue, createNew, callback) {
    if(qKey == null || qValue == null) {
        var message = "Both key and value must be set. You sent, key: " + qKey + " value: " + qValue;
        callback(false, message);
    }
    else {
        this.query = {qKey:qValue};
        this.update = {sKey:sValue};
    }
    dbObj.this.collectionName.update(
        this.query, 
        {$set: this.update},
        {upsert: createNew},
        function(err, result) {
            if (err) {
                callback(false, err);
            };
            callback(true, result);
        }
    );
}